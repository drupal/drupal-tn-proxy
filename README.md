# drupal-tn-proxy

List of TN-enabled Drupal 8 sites.

This repo implements an OpenShift proxy for each TN-enabled Drupal 8 site, defining:
- Route
- Service
- Custom endpoint -> Drupal 8 LBs

## Making a new site TN accessible

1. Ask Stefan to review the access request and require his approval. The present agreement is that
  a site needs to have no public content, but be completely behind the CERN SSO.
2. Create a [new route for the site in dir `tn-site-routes`](tn-site-routes) by copying
  [an existing one](tn-site-routes/cryo4lhc-route.yaml) and changing only the hostname
3. Create the new resource in the OpenShift project (there's no CD)